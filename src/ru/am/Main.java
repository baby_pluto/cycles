package ru.am;

public class Main {
    public static void main(String[] args) {

        System.out.println("Цикл while. 11 членов последовательности Фибоначчи:");
        int current = 1;
        int previous = 0;
        int i = 1;
        while (i <= 11) {
            System.out.print(current + " ");
            current = current + previous;
            previous = current - previous;
            i++;
        }

        System.out.println("\n\nЦикл for. 11 членов последовательности Фибоначчи:");
        current = 1;
        previous = 0;
        for (i = 1; i <= 11; i++) {
            System.out.print(current + " ");
            current = current + previous;
            previous = current - previous;
        }
    }
}